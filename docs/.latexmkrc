# Generate pdf using xelatex
$pdf_mode = 5;

# Use biber for bibliography
$bibtex_use = 2;

# Define xelatex command
$xelatex = "xelatex -interaction=nonstopmode -shell-escape -synctex=1 %O %S";

# Define biber command
$biber = "biber --input-directory=docs %O %S";

# Do not delete generated pdf
$cleanup_mode = 2;

# Delete generated files
$cleanup_includes_generated = 1;

# Define output directory
$aux_dir = "build/docs";
$out_dir = "build/docs";

# Define TEXINPUTS
ensure_path("TEXINPUTS", "docs");

# Define the input files
@default_files = ("docs/thesis.tex");

# vim:ft=perl:
