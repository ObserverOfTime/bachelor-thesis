/*!
 * \file
 * \brief Matrix cryptography implementations
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include "crypto.hpp"

seal::Ciphertext matrix::crypto::flatten(
      seal::Evaluator &eval, const seal::GaloisKeys &keys, const Mat1C &mat) {
    seal::Ciphertext ct_result;
    auto l = static_cast<int32_t>(mat.size());

    Mat1C ct_dist(l);
    ct_dist[0] = mat[0];

    for (int32_t i = 1; i < l; ++i)
        eval.rotate_vector(mat[i], -i * l, keys, ct_dist[i]);

    eval.add_many(ct_dist, ct_result);

    return ct_result;
}
