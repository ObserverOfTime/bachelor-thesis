\begin{tikzpicture}[font=\rmfamily, scale=0.9]
\setlength{\baselineskip}{18pt}

\node[
    rectangle,
    rounded corners,
    align=center,
    inner sep=9pt,
    fill=blue!30] (Mi) at (0, 0) {
        \textbf{Message}\\
        \(\vec{m} \in \CC^{\nu/2}\)};

\node[
    rectangle,
    rounded corners,
    align=center,
    inner sep=9pt,
    fill=orange!50] (Pi) at (6, 0) {
        \textbf{Plaintext}\\
        \(p(x) \in \Ring\)};

\node[
    rectangle,
    rounded corners,
    align=center,
    inner sep=9pt,
    fill=red!40] (Ci) at (12, 0) {
        \textbf{Ciphertext}\\
        \(\vec{c} \in \Ring_q^2\)};

\node[
    rectangle,
    rounded corners,
    align=center,
    inner sep=9pt,
    fill=red!40] (Co) at (12, -4) {
        \textbf{Ciphertext}\\
        \(\vec{c'} = f(\vec{c})\)};

\node[
    rectangle,
    rounded corners,
    align=center,
    inner sep=9pt,
    fill=orange!50] (Po) at (6, -4) {
        \textbf{Plaintext}\\
        \(p' = f(p)\)};

\node[
    rectangle,
    rounded corners,
    align=center,
    inner sep=8pt,
    fill=blue!30] (Mo) at (0, -4) {
        \textbf{Message}\\
        \(\vec{m'} = f(\vec{m})\)};

\draw[
    -latex,
    ultra thick,
    draw=green!60] (Mi) --
    node [midway, above] {encode} (Pi);

\draw[
    -latex,
    ultra thick,
    draw=green!60] (Pi) --
    node [midway, above] {encrypt} (Ci);

\draw[
    -latex,
    ultra thick,
    draw=green!60] (Ci) --
    node [midway, left] {eval $\;f$} (Co);

\draw[
    -latex,
    ultra thick,
    draw=green!60] (Co) --
    node [midway, above] {decrypt} (Po);

\draw[
    -latex,
    ultra thick,
    draw=green!60] (Po) --
    node [midway, above] {decode} (Mo);
\end{tikzpicture}
