/*!
 * \file
 * \brief Matrix utility definitions
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#pragma once

#include <cstdint>
#include <vector>

/*!
 * \brief An 1D matrix of type \p T
 * \tparam T The type of the matrix's values.
 */
template <typename T> using Mat1_ = std::vector<T>;

/*!
 * \brief A 2D matrix of type \p T
 * \tparam T The type of the matrix's values.
 */
template <typename T> using Mat2_ = Mat1_<Mat1_<T>>;

/*!
 * \brief A 3D matrix of type \p T
 * \tparam T The type of the matrix's values.
 */
template <typename T> using Mat3_ = Mat1_<Mat1_<Mat1_<T>>>;

/*!
 * \brief An 1D matrix of type \c double
 */
using Mat1d = Mat1_<double>;

/*! \brief A 2D matrix of type \c double */
using Mat2d = Mat2_<double>;

/*! \brief A 3D matrix of type \c double */
using Mat3d = Mat3_<double>;

/*! \brief Matrix operations */
namespace matrix {
    /*!
     * \brief Fill a matrix with random \c double values
     * \param[out] mat the matrix to be filled
     * \param[in] lower the lower bound of the values
     * \param[in] upper the upper bound of the values
     */
    void random(Mat2d &mat, double lower, double upper);

    /*!
     * \brief Get the nth diagonal of a matrix
     * \param mat a 2D matrix
     * \param n the index of the diagonal
     * \return An 1D matrix
     */
    Mat1d diagonal(const Mat2d &mat, uint32_t n);

    /*!
     * \brief Get all diagonals of a matrix
     * \param mat a 2D matrix
     * \return A 2D matrix
     */
    Mat2d diagonals(const Mat2d &mat);

    /*!
     * Multiply two matrices of type \c double
     * \param left The left-hand matrix
     * \param right The right-hand matrix
     * \return The result of the multiplication
     */
    Mat2d multiply(const Mat2d &left, const Mat2d &right);
}  // namespace matrix
