/*!
 * \file
 * \brief Main executable
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include <exception>
#include <seal/seal.h>

#include "arguments.hpp"
#include "matrix/crypto.hpp"
#include "matrix/permute.hpp"
#include "multiplier.hpp"
#include "prettyprint.hpp"

/*!
 * \brief Logging macro
 *
 * Includes the file and line in debug mode.
 *
 * \param lvl the log level
 * \param msg the log message
 */
#ifndef NDEBUG
#define LOG(lvl, msg) "[" #lvl "] (" __FILE__ ":" << __LINE__ << ") " << (msg)
#else
#define LOG(lvl, msg) "[" #lvl "] " << (msg)
#endif  // !NDEBUG

/*!
 * \brief Run the program
 * \param argc The arguments count
 * \param argv The arguments values
 * \return The exit code
 */
int main(int argc, char **argv) {
    Arguments args {{argc, argv}};

    const uint64_t size = args.size, size_sq = size * size;

    if (args.verbose) {
        std::cout << LOG(VERBOSE, "Size: ") << size << "x" << size
                  << ", Bounds: " << args.lower << "-" << args.upper
                  << std::endl;
    }

    Mat2d A(size, Mat1d(size));
    Mat2d B(size, Mat1d(size));

    matrix::random(A, args.lower, args.upper);
    matrix::random(B, args.lower, args.upper);

#ifndef NDEBUG
    std::cout << LOG(DEBUG, "Matrix A: ") << A << std::endl;
    std::cout << LOG(DEBUG, "Matrix B: ") << B << std::endl;
    std::cout << LOG(DEBUG, "A * B: ") << matrix::multiply(A, B) << std::endl;
#endif  // !NDEBUG

    if (args.verbose) {
        std::cout << LOG(VERBOSE, "Polynomial modulus degree: ")
                  << POLY_MOD_DEGREE << std::endl;
        std::cout << LOG(VERBOSE, "Coefficient modulus: ")
                  << std::vector COEFF_MOD_BITS << std::endl;
    }

    try {
        seal::EncryptionParameters params(seal::scheme_type::ckks);
        params.set_poly_modulus_degree(POLY_MOD_DEGREE);
        params.set_coeff_modulus(
              seal::CoeffModulus::Create(POLY_MOD_DEGREE, COEFF_MOD_BITS));
        seal::SEALContext context(params);

        seal::KeyGenerator keygen(context);
        seal::SecretKey sk = keygen.secret_key();
        seal::PublicKey pk;
        keygen.create_public_key(pk);
        seal::GaloisKeys gal_keys;
        keygen.create_galois_keys(gal_keys);

        seal::Encryptor encryptor(context, pk);
        seal::Decryptor decryptor(context, sk);
        seal::CKKSEncoder encoder(context);

        Mat2d sigma = matrix::diagonals(matrix::permute::sigma(size));
        Mat2d tau = matrix::diagonals(matrix::permute::tau(size));

        Mat3d phi(size - 1, Mat2d(size_sq, Mat1d(size_sq)));
        std::generate(phi.begin(), phi.end(), [size, k = 0]() mutable {
            return matrix::diagonals(matrix::permute::phi(size, ++k));
        });

        Mat3d psi(size - 1, Mat2d(size_sq, Mat1d(size_sq)));
        std::generate(psi.begin(), psi.end(), [size, k = 0]() mutable {
            return matrix::diagonals(matrix::permute::psi(size, ++k));
        });

        Mat1P sigma_pt(size_sq);
        matrix::crypto::encode(encoder, sigma, sigma_pt);

        Mat1P tau_pt(size_sq);
        matrix::crypto::encode(encoder, tau, tau_pt);

        Mat2P phi_pt(size - 1, Mat1P(size_sq));
        for (uint64_t k = 0; k < size - 1; ++k)
            matrix::crypto::encode(encoder, phi[k], phi_pt[k]);

        Mat2P psi_pt(size - 1, Mat1P(size_sq));
        for (uint64_t k = 0; k < size - 1; ++k)
            matrix::crypto::encode(encoder, psi[k], psi_pt[k]);

        Mat1P A_pt(size);
        matrix::crypto::encode(encoder, A, A_pt);
        Mat1C A_ct(size);
        matrix::crypto::encrypt(encryptor, A_pt, A_ct);

        Mat1P B_pt(size);
        matrix::crypto::encode(encoder, B, B_pt);
        Mat1C B_ct(size);
        matrix::crypto::encrypt(encryptor, B_pt, B_ct);

        Multiplier multiply(params, gal_keys);
        seal::Ciphertext AB_ct =
              multiply({A_ct, B_ct}, size, sigma_pt, tau_pt, phi_pt, psi_pt);

        seal::Plaintext AB_pt;
        decryptor.decrypt(AB_ct, AB_pt);

        Mat1d AB_res_1d(size_sq);
        encoder.decode(AB_pt, AB_res_1d);

        Mat2d AB_res_2d(size, Mat1d(size));
        for (uint64_t i = 0; i < size_sq; ++i)
            AB_res_2d[i / size][i % size] = AB_res_1d[i];

        std::cout << LOG(INFO, "CT(A) * CT(B): ") << AB_res_2d << std::endl;
    } catch (const std::exception &exc) {
        std::cerr << LOG(ERROR, exc.what()) << std::endl;
        return 1;
    }

    return 0;
}
