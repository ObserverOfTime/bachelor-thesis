# Bachelor's Thesis

This is my bachelor's thesis for Harokopio University of Athens.<br/>
The topic is &ldquo;Homomorphic encryption using the SEAL library&rdquo;.

## Documentation

The code is documented [here](https://observeroftime.gitlab.io/bachelor-thesis/).

## Compilation

**Requirements:**

<ul>
<li><code>tar</code></li>
<li><code>curl</code></li>
<li><code>cmake</code></li>
<li><code>gcc</code> / <code>clang</code></li>
</ul>

First, download, compile, and install SEAL:

```bash
tar xzf - < <(
  curl -Ss https://codeload.github.com/microsoft/SEAL/tar.gz/v3.7.2)
cmake SEAL-3.7.2 \
  -B SEAL-3.7.2/build \
  -DCMAKE_BUILD_TYPE=Release \
  -DSEAL_BUILD_DEPS=OFF \
  -DSEAL_USE_MSGSL=OFF \
  -DSEAL_USE_ZSTD=OFF \
  -DSEAL_USE_ZLIB=OFF
cmake --build SEAL-3.7.2/build --target install
rm -rf SEAL-3.7.2
```

Then, download and compile the code:

```bash
tar xzf - < <(
  curl -Ss https://gitlab.com/ObserverOfTime/bachelor-thesis/-/archive/master/bachelor-thesis-master.tar.gz)
cd bachelor-thesis-master
cmake -B build/debug -DCMAKE_BUILD_TYPE=Debug
cmake --build build/debug --target main
```

Finally, you can run the executable:

```bash
./build/debug/main --help
```
