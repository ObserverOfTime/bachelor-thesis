/*!
 * \file
 * \brief Linear Transformer class implementation
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include "linear_transformer.hpp"

seal::Ciphertext LinearTransformer::operator()(
      const seal::Ciphertext &ciphertext, const Mat1P &matrix) {
    seal::SEALContext ctx(params_);
    seal::Evaluator eval(ctx);
    seal::Ciphertext ct_rot, ct_new, ct_result, temp_rot;

    auto l = static_cast<int32_t>(matrix.size());

    eval.rotate_vector(ciphertext, -l, keys_, ct_rot);
    eval.add(ciphertext, ct_rot, ct_new);

    Mat1C ct_dist(l);
    eval.multiply_plain(ct_new, matrix[0], ct_dist[0]);

    for (int32_t i = 1; i < l; ++i) {
        eval.rotate_vector(ct_new, i, keys_, temp_rot);
        eval.multiply_plain(temp_rot, matrix[i], ct_dist[i]);
    }

    eval.add_many(ct_dist, ct_result);

    return ct_result;
}
