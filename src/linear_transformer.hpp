/*!
 * \file
 * \brief Linear Transformer class declaration
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#pragma once

#include "matrix/crypto.hpp"

/*! \brief Linear Transformer class */
class LinearTransformer final {
public:
    /*!
     * \brief Create an instance of a Linear Transformer
     * \param params the encryption parameters used for the transformation
     * \param keys the Galois keys used for the transformation
     */
    explicit LinearTransformer(
          const seal::EncryptionParameters &params,
          const seal::GaloisKeys &keys):
        params_(params), keys_(keys) {}

    /*!
     * \brief Transform a ciphertext using a matrix of type \c seal::Plaintext
     * \param ciphertext the ciphertext to be transformed
     * \param matrix the matrix used for the transformation
     * \return The generated \c seal::Ciphertext
     */
    seal::Ciphertext operator()(
          const seal::Ciphertext &ciphertext, const Mat1P &matrix);

private:
    /*! \brief The encryption parameters of the transformer */
    const seal::EncryptionParameters &params_;

    /*! \brief The Galois keys used by the transformer */
    const seal::GaloisKeys &keys_;
};  // class LinearTransformer
