/*!
 * \file
 * \brief Matrix cryptography definitions
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#pragma once

#include <cassert>
#include <seal/seal.h>

#include "utils.hpp"

#ifndef POLY_MOD_DEGREE
/*! \brief The polynomial modulus degree */
#define POLY_MOD_DEGREE 16384
#endif  // !POLY_MOD_DEGREE

#ifndef ENCODING_SCALE
/*! \brief The scale of the encoder */
#define ENCODING_SCALE (1UL << 40)
#endif  // !ENCODING_SCALE

#ifndef COEFF_MOD_BITS
// clang-format off
/*! \brief The coefficient modulus bit-lengths */
#define COEFF_MOD_BITS {60, 40, 40, 40, 40, 60}
// clang-format on
#endif  // !COEFF_MOD_BITS

/*! \brief An 1D matrix of type \c seal::Plaintext */
using Mat1P = Mat1_<seal::Plaintext>;

/*! \brief A 2D matrix of type \c seal::Plaintext */
using Mat2P = Mat2_<seal::Plaintext>;

/*! \brief An 1D matrix of type \c seal::Ciphertext */
using Mat1C = Mat1_<seal::Ciphertext>;

/*! \brief Cryptographic operations on matrices */
namespace matrix::crypto {
    /*!
     * \brief Encode a 2D \c double matrix
     * \param[in] enc a CKKS encoder
     * \param[in] src the input matrix
     * \param[out] dst the output matrix
     */
    inline void encode(seal::CKKSEncoder &enc, Mat2d &src, Mat1P &dst) {
        assert(src.size() == dst.size());
        for (uint64_t i = 0, l = dst.size(); i < l; ++i)
            enc.encode(src[i], ENCODING_SCALE, dst[i]);
    }

    /*!
     * \brief Encrypt a \c seal::Plaintext matrix
     * \param[in] enc an encryptor
     * \param[in] src the input matrix
     * \param[out] dst the output matrix
     */
    inline void encrypt(seal::Encryptor &enc, Mat1P &src, Mat1C &dst) {
        assert(src.size() == dst.size());
        for (uint64_t i = 0, l = dst.size(); i < l; ++i)
            enc.encrypt(src[i], dst[i]);
    }

    /*!
     * \brief Flatten a matrix into a single \c seal::Ciphertext
     * \param eval the evaluator used for the operations
     * \param keys the Galois keys used by the evaluator
     * \param mat a matrix of ciphertexts
     * \return the flattened ciphertext
     */
    seal::Ciphertext flatten(
          seal::Evaluator &eval, const seal::GaloisKeys &keys,
          const Mat1C &mat);
}  // namespace matrix::crypto
