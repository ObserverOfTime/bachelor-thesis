\section{Αλγόριθμος Πολλαπλασιασμού}\label{sec:multiplication}

Ο πολλαπλασιασμός κρυπτογραφημένων πινάκων μεγέθους
$d \times d$ μπορεί να εκτελεστεί όπως ακριβώς
και ο γνωστός πολλαπλασιασμός μη κρυπτογραφημένων
πινάκων, με πολυπλοκότητα τάξεως $\symcal{O}(d^3)$.
Όμως, οι \citeauthor{jiang2018secure} έδειξαν πως
μπορεί να βελτιστοποιηθεί ο αλγόριθμος αναπαριστώντας
τον κάθε πίνακα με ένα μόνο κρυπτοκείμενο (έναντι
$d^2$), αξιοποιώντας την πράξη της περιστροφής.
Η μέθοδος λειτουργεί με τρόπο \acs{SIMD} και
έχει πολυπλοκότητα τάξεως $\symcal{O}(d)$.

\vspace{-1em}
\subsection{Συμβολισμοί}\label{subsec:mult-notation}

\begin{description}
    \item[$\widehat{A}$]
        Αναπαριστά την πράξη της μετατροπής
        (κατά σειρά) ενός πίνακα $A$ διαστάσεων
        $m \times n$ σε διάνυσμα μεγέθους $m \cdot n$.
        Αντιστοιχεί στη συνάρτηση $\symtt{vec}(A^\top)$.
        \cite[\S16.2]{harville1997kronecker}
    \item[$A \circ \vec{v}$]
        Συμβολίζει το γινόμενο Hadamard
        \cite{million2007hadamard} μεταξύ ενός
        πίνακα $A$ διαστάσεων $m \times n$ και
        ενός διανύσματος $\vec{v}$ μεγέθους $n$.
    \item[$\llbracket{P}\rrbracket$]
        Οι αγκύλες Iverson \cite[\S1]{knuth1992two}
        χρησιμοποιούνται για την αντιστοίχηση
        μίας λογικής έκφρασης $P$ στον αριθμό $1$
        αν αυτή είναι αληθής ή $0$ αν είναι ψευδής.
        Για λόγους σαφήνειας αξιοποιώ
        διπλές αγκύλες αντί για μονές.
\end{description}

\vspace{-1em}
\subsection{Τετραγωνικοί Πίνακες}\label{subsec:mult-square}

Έστω δύο κρυπτογραφημένοι \acap{matrix} $A$, $B$ μεγέθους
$d \times d$ με $d = 3$ (\autoref{fig:example-matrices}).

Ως $\symtt{Rot}(C; \vartheta)$ ορίζεται η συνάρτηση
περιστροφής ενός ciphertext $C$ κατά $|\vartheta|$
βήματα, αριστερόστροφα αν $\vartheta > 0$ ή δεξιόστροφα
αν $\vartheta < 0$. \cite[\S2.2]{jiang2018secure}

Επειδή το κάθε \acs{ciphertext} αντιστοιχεί σε ένα
\aca{vector}, μπορούμε να μετατρέψουμε τους \acap{matrix}
σε \acap{vector} μεγέθους \mbox{$n = d^2$}, ώστε να
ελαττώσουμε τον καθένα σε ένα μόνο \acs{ciphertext}
αντί για $d$. \cite[\S3.2]{jiang2018secure}

Η μετατροπή αυτή επιτυγχάνεται για την κάθε
σειρά $i$ του \acag{matrix} $A$---και παρομοίως
του $B$---εφαρμόζοντας $\symtt{Rot}(A; d \cdot i)$
και προσθέτοντας τα παραγόμενα \acap{vector} όπως
φαίνεται στο ακόλουθο \autoref{fig:matrix-to-vector}.

\vspace{0.5em}
\begin{figure}[hb!]
    \begin{minipage}{0.3\linewidth}
        \caption{Πίνακες $3 \times 3$}\label{fig:example-matrices}
        \begin{align*}
            A = \begin{array}{|c|c|c|}
                    \hline
                    1 & 2 & 3\\\hline
                    4 & 5 & 6\\\hline
                    7 & 8 & 9\\\hline
            \end{array}
            \\\vspace{0.2em}
            B = \begin{array}{|c|c|c|}
                    \hline
                    9 & 8 & 7\\\hline
                    6 & 5 & 4\\\hline
                    3 & 2 & 1\\\hline
            \end{array}
        \end{align*}
    \end{minipage}
    \hfill\vline\hfill
    \begin{minipage}{0.65\linewidth}
        \caption{Μετατροπή πίνακα σε διάνυσμα}\label{fig:matrix-to-vector}
        \begin{equation*}
            \scalebox{1.25}{\(
                \begin{array}{c}
                    \\\rotationarrow
                    \\\rotationarrow
                \end{array}
                \hspace{-0.5em}
                \begin{array}{|c|c|c|}
                    \hline
                    1 & 2 & 3\\\hline
                    4 & 5 & 6\\\hline
                    7 & 8 & 9\\\hline
                \end{array}
            \)}
            \quad\scalebox{1.75}{$\Rightarrow$}\quad
            \begin{gathered}
                \begin{array}{|c|c|c|c|c|c|c|c|c|}
                    \hline
                    1 & 2 & 3 &
                    \nil & \nil & \nil &
                    \nil & \nil & \nil\\\hline
                \end{array}
                \vspace{-0.2em}\\\scalebox{1.25}{$+$}\vspace{-0.25em}\\
                \begin{array}{|c|c|c|c|c|c|c|c|c|}
                    \hline
                    \nil & \nil & \nil &
                    4 & 5 & 6 &
                    \nil & \nil & \nil\\\hline
                \end{array}
                \vspace{-0.2em}\\\scalebox{1.25}{$+$}\vspace{-0.25em}\\
                \begin{array}{|c|c|c|c|c|c|c|c|c|}
                    \hline
                    \nil & \nil & \nil &
                    \nil & \nil & \nil &
                    7 & 8 & 9\\\hline
                \end{array}
                \vspace{-0.2em}\\\scalebox{1.25}{$=$}\vspace{-0.25em}\\
                \begin{array}{|c|c|c|c|c|c|c|c|c|}
                    \hline
                    1 & 2 & 3 &
                    4 & 5 & 6 &
                    7 & 8 & 9\\\hline
                \end{array}
            \end{gathered}
        \end{equation*}
    \end{minipage}
\end{figure}
\clearpage

Για τις μεταθέσεις $\sigma$, $\tau$, $\phi^k$, $\psi^k$
ορίζουμε τους \acap{matrix} $U$, $Y$, $V^k$, $W^k$
(μεγέθους $n \times n$) αντίστοιχα, σύμφωνα με τους
παρακάτω τύπους. \cite[\S3.3.1]{jiang2018secure}

\vspace{-0.5em}
\begin{alignat*}{4}
    &\sigma &&\colon U_{\dijl} &&=
        \llbracket l = d \cdot i + \modd{i+j} &&\rrbracket\\
    &\tau &&\colon Y_{\dijl} &&=
        \llbracket l = d \cdot \modd{i+j} + j &&\rrbracket\\
    &\phi^k &&\colon V_{\dijl}^k &&=
        \llbracket l = d \cdot i + \modd{j+k} &&\rrbracket\\
    &\psi^k &&\colon W_{\dijl}^k &&=
        \llbracket l = d \cdot \modd{i+k} + j &&\rrbracket
\end{alignat*}

Έπειτα, μετασχηματίζουμε τους \acap{matrix}
\mbox{$A \to A'$} και \mbox{$B \to B'$}
πολλαπλασιάζοντας τον κάθε \acag{matrix}
\acag{permutation} με το αντίστοιχο \aca{vector}.
\cite[\S2.3~\S3.3.2]{jiang2018secure}

\vspace{0.2em}
\begin{itemize}[label=\rtriangle\,]
    \setlength{\itemindent}{0.5em}
    \item $A'_0 = U \circ \widehat{A}$ (\autoref{fig:sigma-permutation})
    \item $B'_0 = Y \circ \widehat{B}$ (\autoref{fig:tau-permutation})
    \item $\forall k \in [1, d)\;A'_k = V^k \circ A'_0$ (\autoref{fig:phi-permutation})
    \item $\forall k \in [1, d)\;B'_k = W^k \circ B'_0$ (\autoref{fig:psi-permutation})
\end{itemize}
\vspace{0.2em}

Τέλος, ορίζουμε το διάνυσμα $\widehat{AB}$ ως το
άθροισμα των πολλαπλασιασμών των επιμέρους διανυσμάτων
και το μετατρέπουμε ξανά σε πίνακα για να λάβουμε το
αποτέλεσμα (\autoref{fig:vector-multiplication}).

\vspace{0.2em}
\[
    \widehat{AB} = \sum_{k=0}^d A'_k \cdot B'_k
\]

\vspace{1em}
\begin{figure}[hb!]
    \begin{minipage}{0.475\linewidth}
        \caption{Γραμμικός μετασχηματισμός ($\sigma$)}\label{fig:sigma-permutation}
        \begin{gather*}
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$\times$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$=$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                1 & 2 & 3 & 5 & 6 & 4 & 9 & 7 & 8\\\hlineB{2}
            \end{array}
        \end{gather*}
    \end{minipage}
    \hfill\vline\hfill
    \begin{minipage}{0.475\linewidth}
        \caption{Γραμμικός μετασχηματισμός ($\tau$)}\label{fig:tau-permutation}
        \begin{gather*}
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\\hlineB{2}
                0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\\hline
                0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\\hline
                0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$\times$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                9 & 8 & 7 & 6 & 5 & 4 & 3 & 2 & 1\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$=$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                9 & 5 & 1 & 6 & 2 & 7 & 3 & 8 & 4\\\hlineB{2}
            \end{array}
        \end{gather*}
    \end{minipage}
\end{figure}
\clearpage

\begin{figure}[ht!]
    \caption{Γραμμικός μετασχηματισμός ($\phi^k$)}\label{fig:phi-permutation}
    \begin{subfigure}[h]{0.45\linewidth}
        \caption{$k = 1$}
        \begin{gather*}
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$\times$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                1 & 2 & 3 & 5 & 6 & 4 & 9 & 7 & 8\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$=$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                2 & 3 & 1 & 6 & 4 & 5 & 7 & 8 & 9\\\hlineB{2}
            \end{array}
        \end{gather*}
    \end{subfigure}
    \begin{subfigure}[h]{0.45\linewidth}
        \caption{$k = 2$}
        \begin{gather*}
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$\times$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                1 & 2 & 3 & 5 & 6 & 4 & 9 & 7 & 8\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$=$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                3 & 1 & 2 & 4 & 5 & 6 & 8 & 9 & 7\\\hlineB{2}
            \end{array}
        \end{gather*}
    \end{subfigure}
\end{figure}

\vspace{2em}
\begin{figure}[hb!]
    \caption{Γραμμικός μετασχηματισμός ($\psi^k$)}\label{fig:psi-permutation}
    \begin{subfigure}[h]{0.45\linewidth}
        \caption{$k = 1$}
        \begin{gather*}
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\\hlineB{2}
                1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$\times$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                9 & 5 & 1 & 6 & 2 & 7 & 3 & 8 & 4\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$=$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                6 & 2 & 7 & 3 & 8 & 4 & 9 & 5 & 1\\\hlineB{2}
            \end{array}
        \end{gather*}
    \end{subfigure}
    \begin{subfigure}[h]{0.45\linewidth}
        \caption{$k = 2$}
        \begin{gather*}
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\\hlineB{2}
                1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\\hlineB{2}
                0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\\hline
                0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$\times$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                9 & 5 & 1 & 6 & 2 & 7 & 3 & 8 & 4\\\hlineB{2}
            \end{array}
            \\\scalebox{1.5}{$=$}\\
            \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
                \hlineB{2}
                3 & 8 & 4 & 9 & 5 & 1 & 6 & 2 & 7\\\hlineB{2}
            \end{array}
        \end{gather*}
    \end{subfigure}
\end{figure}
\clearpage

\begin{figure}[ht!]
    \caption{Πολλαπλασιασμός διανυσμάτων}\label{fig:vector-multiplication}
    \begin{gather*}
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            1 & 2 & 3 & 5 & 6 & 4 & 9 & 7 & 8\\\hlineB{2}
        \end{array}
        \;\scalebox{1.25}{$\times$}\;
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            9 & 5 & 1 & 6 & 2 & 7 & 3 & 8 & 4\\\hlineB{2}
        \end{array}
        % 9 & 10 & 3 & 30 & 12 & 28 & 27 & 56 & 32
        \\\scalebox{1.25}{$+$}\\
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            2 & 3 & 1 & 6 & 4 & 5 & 7 & 8 & 9\\\hlineB{2}
        \end{array}
        \;\scalebox{1.25}{$\times$}\;
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            6 & 2 & 7 & 3 & 8 & 4 & 9 & 5 & 1\\\hlineB{2}
        \end{array}
        % 21 & 16 & 10 & 48 & 44 & 48 & 90 & 96 & 41
        \\\scalebox{1.25}{$+$}\\
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            3 & 1 & 2 & 4 & 5 & 6 & 8 & 9 & 7\\\hlineB{2}
        \end{array}
        \;\scalebox{1.25}{$\times$}\;
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            3 & 8 & 4 & 9 & 5 & 1 & 6 & 2 & 7\\\hlineB{2}
        \end{array}
        \\\scalebox{1.25}{$=$}\\
        \begin{array}{V{2}c|c|cV{2}c|c|cV{2}c|c|cV{2}}
            \hlineB{2}
            30 & 24 & 18 & 84 & 69 & 54 & 138 & 114 & 90\\\hlineB{2}
        \end{array}
        \\\scalebox{1.5}{$\downarrow$}\\
        \begin{array}{|c|c|c|}
            \hline
            30 & 24 & 18\\\hline
            84 & 69 & 54\\\hline
            138 & 114 & 90\\\hline
        \end{array}
    \end{gather*}
\end{figure}

\subsection{Ορθογώνιοι Πίνακες}\label{subsec:mult-rect}

Εάν ο πίνακας $A$ δεν είναι τετραγωνικός, αλλά έχει
μέγεθος \mbox{$l \times d$} με \mbox{$l < d$}, μπορεί να
μετατραπεί αφελώς σε τετραγωνικό γεμίζοντάς τον με μηδενικά
και να εκτελεστεί ο πολλαπλασιασμός όπως ακριβώς φαίνεται
παραπάνω, με πολυπλοκότητα της τάξεως $\symcal{O}(d)$.
Σε περίπτωση, όμως, που ο αριθμός σειρών $l$ είναι
διαιρέτης του αριθμού στηλών $d$, υπάρχει δυνατότητα
μείωσης της πολυπλοκότητας του αλγορίθμου στην τάξη
$\symcal{O}(l)$. \cite[\S4.2]{jiang2018secure}

Αυτό επιτυγχάνεται δημιουργώντας πρώτα έναν πίνακα
$\underline{A}$ ο οποίος περιέχει \mbox{$d / l$}
κάθετα συνδεδεμένα αντίγραφα του αρχικού πίνακα $A$.

\vspace{0.2em}
\[
    \underline{A} = \left.
        \begin{bmatrix}A\\\cdots\\A\end{bmatrix}
    \right\} \frac{d}{l}
\]

Έπειτα, εκτελούνται τα βήματα του παραπάνω
αλγορίθμου, αντικαθιστώντας τον πίνακα $A$ με
τον $\underline{A}$ και το μέγεθος $d$ με το $l$.
Το τελικό αποτέλεσμα δίνεται με ένα επιπλέον βήμα:

\vspace{0.2em}
\[
    \widehat{AB} = \sum_{k=0}^{\nu - 1}\symtt{Rot}
    (\widehat{\underline{A}B}; l \cdot d \cdot 2^k),
    \;\nu = \log_2\left(\frac{d}{l}\right)
\]
