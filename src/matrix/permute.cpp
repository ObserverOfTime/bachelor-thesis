/*!
 * \file
 * \brief Matrix permutation implementations
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include "permute.hpp"

#include <cassert>

/*!
 * \brief Perform the permutation
 * \param[in] expr an integer expression
 * \param[out] result the matrix to store the result
 */
#define __PERMUTE(expr, result)                                      \
    do {                                                             \
        for (uint32_t l = 0; l < n; ++l)                             \
            for (uint32_t i = 0; i < d; ++i)                         \
                for (uint32_t j = 0; j < d; ++j)                     \
                    (result)[d * i + j][l] = l == (expr) ? 1 : 1e-9; \
    } while (false)

Mat2d matrix::permute::sigma(uint64_t d) {
    uint64_t n = d * d;
    Mat2d res(n, Mat1d(n));
    __PERMUTE(d * i + ((i + j) % d), res);
    return res;
}

Mat2d matrix::permute::tau(uint64_t d) {
    uint64_t n = d * d;
    Mat2d res(n, Mat1d(n));
    __PERMUTE(d * ((i + j) % d) + j, res);
    return res;
}

Mat2d matrix::permute::phi(uint64_t d, uint32_t k) {
    assert(k >= 1 && k < d);
    uint64_t n = d * d;
    Mat2d res(n, Mat1d(n));
    __PERMUTE(d * i + ((j + k) % d), res);
    return res;
}

Mat2d matrix::permute::psi(uint64_t d, uint32_t k) {
    assert(k >= 1 && k < d);
    uint64_t n = d * d;
    Mat2d res(n, Mat1d(n));
    __PERMUTE(d * ((i + k) % d) + j, res);
    return res;
}
