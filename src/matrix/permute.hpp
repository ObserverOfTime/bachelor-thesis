/*!
 * \file
 * \brief Matrix permutation definitions
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#pragma once

#include "utils.hpp"

/*! \brief Permutations defined in \cite jiang2018secure */
namespace matrix::permute {
    /*!
     * \brief The &sigma; permutation
     * \param d the dimensions of the original matrix
     * \return A 2D binary matrix
     */
    Mat2d sigma(uint64_t d);

    /*!
     * \brief The &tau; permutation
     * \param d the dimensions of the original matrix
     * \return A 2D binary matrix
     */
    Mat2d tau(uint64_t d);

    /*!
     * \brief The &phi; permutation
     * \param d the dimensions of the original matrix
     * \param k an integer such that \f$k \in [1, d)\f$
     * \return A 2D binary matrix
     */
    Mat2d phi(uint64_t d, uint32_t k);

    /*!
     * \brief The &psi; permutation
     * \param d the dimensions of the original matrix
     * \param k an integer such that \f$k \in [1, d)\f$
     * \return A 2D binary matrix
     */
    Mat2d psi(uint64_t d, uint32_t k);
}  // namespace matrix::permute
