\subsection{Σύστημα CKKS}\label{subsec:ckks-scheme}

Το σύστημα ομομορφικής κρυπτογράφησης \acf{CKKS}
κατασκευάστηκε το 2017 με στόχο την επίτευξη
προσεγγιστικών πράξεων σε κρυπτοκείμενα.
Υποστηρίζει τις πράξεις της πρόσθεσης
και του πολλαπλασιασμού κρυπτοκειμένων
κατά προσέγγιση, χρησιμοποιώντας μια νέα
διαδικασία ανακλιμάκωσης (rescaling) για τη
διαχείριση του  μεγέθους του αρχικού μηνύματος.
Αυτή η διαδικασία μειώνει τον \acag{modulus} του
κρυπτοκειμένου, στρογγυλοποιώντας έτσι το αρχικό μήνυμα.
Ως αποτέλεσμα, η αποκρυπτογράφηση εξάγει μία προσέγγιση
της τιμής του μηνύματος με προκαθορισμένη ακρίβεια.

\begingroup
\renewcommand*{\mkbibbrackets}[1]{(#1)}
Το σύστημα CKKS έχει υλοποιηθεί από διάφορες
βιβλιοθήκες ανοικτού κώδικα στη γλώσσα C++
\cite{heaan,helib,palisade,sealcrypto}
και στη γλώσσα Go \cite{lattigo}.
Η βιβλιοθήκη SEAL, συγκεκριμένα, χρησιμοποιήθηκε
σε αυτή την εργασία και περιγράφεται αργότερα.
\endgroup

Το \autoref{fig:ckks-overview} παρέχει μία
γενική επισκόπηση του συστήματος CKKS\@.
Το σύστημα χρησιμοποιεί πολυωνυμικούς
δακτυλίους διότι προσφέρουν μια αρκετά
καλή ισορροπία μεταξύ ασφάλειας και απόδοσης.
Αρχικά, λαμβάνει ένα διάνυσμα $\vec{m}$, στο
οποίο αναμένεται να εκτελέσει κάποιον υπολογισμό.
Στη συνέχεια, το κωδικοποιεί σε ένα πολυώνυμο
$p(x)$, το οποίο και κρυπτογραφεί σε ένα διάνυσμα
$\vec{c}$ που αποτελεί ένα ζεύγος πολυωνύμων.
Έπειτα, εκτελεί σε αυτό κάποια ομομορφική πράξη
$f$ (όπως πρόσθεση, πολλαπλασιασμό, ή περιστροφή),
η οποία παράγει ένα νέο διάνυσμα $\vec{c'}$.
Τέλος, αποκρυπτογραφεί το $\vec{c'}$ στο
πολυώνυμο $p'$, το οποίο αποκωδικοποιεί
για να δώσει το αποτέλεσμα ως το διάνυσμα
$\vec{m'}$. \cite{openmined2020ckks}

\vspace{0.5em}
\begin{figure}[hb!]\centering
    \caption{Επισκόπηση του CKKS}\label{fig:ckks-overview}
    \vspace{0.5ex}\input{imgs/ckks-diagram.tikz}
\end{figure}

\clearpage
\subsubsection{Μαθηματικό Υπόβαθρο}\label{subsubsec:ckks-rlwe}

Η ασφάλεια του συστήματος CKKS βασίζεται
στη δυσκολία του προβλήματος \acf{RLWE},
το οποίο εικάζεται ότι δεν μπορεί να
επιλυθεί εύκολα ούτε σε κβαντικό υπολογιστή.
Το πρόβλημα RLWE αποτελεί ειδίκευση του
\acs{LWE} με τη χρήση πολυωνυμικών δακτυλίων
πάνω σε πεπερασμένα πεδία και δημιουργεί κλειδιά
πολύ μικρότερου μεγέθους\footnotemark~από αυτό.

\footnotetext{Παραμένουν, όμως, μεγαλύτερα από τα
κλειδιά παραδοσιακών αλγορίθμων όπως RSA και \acs{ECDH}.}

Αρχικά, επιλέγονται ομοιόμορφα δύο πολυώνυμα $a$ και
$s$ από έναν δακτύλιο $\Ring_q = \ZZ_q[x]/f(x)$, όπου
$f$ είναι ένα αμείωτο πολυώνυμο βαθμού $\nu - 1$.
Επίσης, λαμβάνεται ένα πολυώνυμο $e$ βαθμού $\nu$
από την κατανομή λαθών $\symcal{X}$, η οποία
αποτελεί συνήθως μία διακριτή Gaussian κατανομή
$\symcal{X}_\sigma$ με τυπική απόκλιση $\sigma$.
Η κατανομή $\symcal{A}_{s,\symcal{X}}$ στον χώρο
$\Ring_q \times \Ring_q$ αποτελείται από πλειάδες
$(a, t)$ όπου $t = a \cdot s + e \in \symcal{R}_q$.
Το πρόβλημα αναφέρεται στην εύρεση του $s$ δοθέντος ενός
πολυωνυμικού πλήθους δειγμάτων $(a, t)$ από την κατανομή
$\symcal{A}_{s,\symcal{X}}$, κάτι που έχει αποδειχθεί πως
είναι εξαιρετικά δύσκολο. \cite[\S2.1]{clercq2015efficient}

\subsubsection{Συμβολισμοί}\label{subsubsec:ckks-notation}

Οι ακόλουθοι συμβολισμοί χρησιμοποιούνται για την περιγραφή
της υλοποίησης του συστήματος. \cites[\S2.1~\S3.4]{
cheon2017homomorphic}[\S A.4]{gentry2015homomorphic}

\begin{description}[labelindent=\parindent]
    \item[$\langle\vec{v}, \vec{w}\rangle$]
        Το εσωτερικό γινόμενο δύο διανυσμάτων
        $\vec{v}$ και $\vec{w}$.
    \item[$\overline{z}$]
        Ο συζυγής αριθμός του μιγαδικού $z$.
    \item[$\lfloor r \rceil$]
        Η στρογγυλοποίηση ενός πραγματικού αριθμού
        $r$ στον κοντινότερό του ακέραιο\linebreak
        (προς τα πάνω σε περίπτωση ισοβαθμίας).
    \item[$S^n$]
        Το Καρτεσιανό γινόμενο ενός συνόλου $S$ με
        τον εαυτό του εκτελεσμένο $n$ φορές.\linebreak
        (π.χ.~~\(S^2 = S_1 \times S_2 = \{(s_1, s_2)
                \,|\, s_1 \in S_1, s_2 \in S_2\}\)~)
    \item[$g \circ f$]
        Η σύνθεση συνάρτησης $g(f(x))$ από
        δύο συναρτήσεις $g(x)$ και $f(x)$.
    \item[$s \gets \symcal{D}$]
        Η λήψη ενός τυχαίου δείγματος $s$
        από μία κατανομή $\symcal{D}$.
    \item[$\symcal{DG}(\sigma^2)$]
        Για έναν πραγματικό αριθμό \mbox{$\sigma > 0$},
        η συνάρτηση λαμβάνει ένα διάνυσμα
        από την κανονική (Gaussian) κατανομή
        $\symcal{N}(0, \sigma^2)^\nu$, με μέση
        τιμή $0$ και απόκλιση $\sigma^2$, και το
        στρογγυλοποιεί στον κοντινότερο ακέραιο.
    \item[$\symcal{HWT}(h)$]
        Για έναν ακέραιο αριθμό \mbox{$h \geq 0$},
        η συνάρτηση λαμβάνει ένα διάνυσμα από το
        σύνολο $\{0, \pm 1\}^\nu$ εφόσον αυτό έχει
        βάρος Hamming (πλήθος τιμών $\pm 1$) ίσο με $h$.
    \item[$\symcal{ZO}(\rho)$]
        Για έναν πραγματικό αριθμό \mbox{$\rho \in [0, 1]$},
        η συνάρτηση λαμβάνει ένα διάνυσμα από το σύνολο
        $\{0, \pm 1\}^\nu$ με πιθανότητα $\rho/2$ για
        τις τιμές $\pm 1$ και $1 - \rho$ για την τιμή $0$.
\end{description}

\clearpage
\subsubsection{Υλοποίηση}\label{subsubsec:ckks-impl}

Έστω $L$ το υπολογιστικό βάθος του κυκλώματος και
\mbox{$\ell \in [0, L]$} ένα επίπεδο εντός αυτού.
Ο χώρος των μηνυμάτων ορίζεται από τον δακτύλιο
\mbox{$\Ring_q = \ZZ_q[x]/(x^\nu + 1)$} που αναπαριστά
το σύνολο των κυκλοτομικών πολυωνύμων βαθμού $\nu$
(δύναμη του $2$), όπου $q$ είναι ο \aca{modulus}.
Ορίζονται, επίσης, ένα πεδίο μιγαδικών αριθμών
\mbox{\(\HH = \{(z_j)_{j \in \ZZ_\mu^\ast}\,|\,
    z_j = \overline{z_{-j}}\} \subset \CC^\nu\)}
και ένα πολλαπλασιαστικό υποσύνολο
\mbox{$T \subset \ZZ_\mu^\ast$} τέτοιο
ώστε \mbox{$\ZZ_\mu^\ast/T = \{\pm 1\}$},
για κάποιον ακέραιο αριθμό $\mu$.
Επιπλέον, δηλώνεται ο ισομορφισμός \mbox{\(
\varpi : (z_j)_{j \in \ZZ_\mu^\ast}
\mapsto (z_j)_{j \in T}\)} και η εμφύτευση
\mbox{$\varsigma : \symcal{R} \to \CC^{\nu}$}.
Ακόμα, η n-οστή ρίζα της μονάδας απεικονίζεται
ως \mbox{$\zeta_n = e^{2\pi i/n}$} και ο
παράγοντας ανακλιμάκωσης του μηνύματος πριν
τη στρογγυλοποίηση ως \mbox{$\varDelta \geq 1$}.
\cite[\S2.2~\S3.2--3.4]{cheon2017homomorphic}

\begin{description}[labelindent=\parindent]
    \item[Δημιουργία κλειδιών:\quad
          $\symtt{KeyGen}(1^\lambda)$]\hfill\\
          \vspace{-1.75em}
          \begin{itemize}[label=\square, leftmargin=*]
              \item Δοθείσας μίας παραμέτρου ασφαλείας
                    $\lambda$, επιλέγονται οι αριθμοί
                    \mbox{$\mu = \mu(\lambda, q_L) \in \ZZ$}
                    (δύναμη του $2$),
                    \mbox{$h = h(\lambda, q_L) \in \ZZ$},
                    \mbox{$p = p(\lambda, q_L) \in \ZZ$} και
                    \mbox{$\sigma = \sigma(\lambda, q_L) \in \RR$}.
              \item Λαμβάνονται τα τυχαία δείγματα
                    \mbox{$s \gets \symcal{HWT}(h)$},
                    \mbox{$a \gets \Ring_{q_L}$},
                    \mbox{$e \gets \symcal{DG}(\sigma^2)$},
                    \mbox{$b \gets -as + e \;(\bmod\; q_L)$}
                    και θέτονται το μυστικό κλειδί \mbox{
                    $sk \gets (1, s)$} και το δημόσιο κλειδί
                    \mbox{$pk \gets (b, a) \in \Ring_{q_L}^2$}.
              \item Λαμβάνονται, επίσης, τα
                    \mbox{$a' \gets \Ring_{p \cdot q_L}$},
                    \mbox{$e' \gets \symcal{DG}(\sigma^2)$},
                    \mbox{\(b' \gets -a's + e' +
                        ps^2 \;(\bmod\; p \cdot q_L)\)}
                    και τίθεται το κλειδί αποτίμησης
                    \mbox{\(evk \gets (b', a')
                        \in \Ring_{p \cdot q_L}^2\)}.
          \end{itemize}
    \item[Κωδικοποίηση:\quad
          $\symtt{Ecd}(\vec{z}; \varDelta)$]\hfill\\
          Για ένα διάνυσμα ακεραίων του Gauss
          $\vec{z} = (z_j)_{j \in T} \in \ZZ [i]^{\nu/2}$
          (μεγέθους $\nu/2$), η συνάρτηση επιστρέφει το
          πολυώνυμο \mbox{\(m(x) = \varsigma^{-1}(
              \lfloor \varDelta \cdot \varpi^{-1}(\vec{z})
              \rceil_{\varsigma(\Ring)}) \in \Ring\)}.
    \item[Αποκωδικοποίηση:\quad
          $\symtt{Dcd}(m; \varDelta)$]\hfill\\
          Η κωδικοποίηση αντιστρέφεται εκτελώντας \mbox{
          $\varpi \circ \varsigma(m)$} και η συνάρτηση
          επιστρέφει ένα διάνυσμα \mbox{\(\vec{z} =
              (z_j)_{j \in T} \in \ZZ [i]^{\nu/2}\)} με
          \mbox{\(z_j = \lfloor \varDelta^{-1} \cdot m(
              \zeta_\mu^j) \rfloor\) για κάθε $j \in T$}.
    \item[Κρυπτογράφηση:\quad
          $\symtt{Enc}_{pk}(m)$]\hfill\\
          Η συνάρτηση λαμβάνει τα δείγματα \mbox{
          $u \gets \symcal{ZO}(0.5)$}, \mbox{
          $e_0, e_1 \gets \symcal{DG}(\sigma^2)$}
          και επιστρέφει \mbox{\(\vec{c} = u \cdot
              pk + (m + e_0,e_1) \;(\bmod\; q_L)\)}.
    \item[Αποκρυπτογράφηση:\quad
          $\symtt{Dec}_{sk}(\vec{c})$]\hfill\\
          Για \mbox{$\vec{c} = (b, a)$}, επιστρέφει
          \mbox{$m = b + a \cdot sk \;(\bmod\; q_\ell)$}.
    \item[Πρόσθεση:\quad
          $\symtt{Add}(\vec{c}_1, \vec{c}_2)$]\hfill\\
          Για \mbox{\(\vec{c}_1, \vec{c}_2 \in
              \Ring_{q_\ell}^2\)}, επιστρέφει
          \mbox{\(\vec{c}_{\symsf{add}} \gets
              \vec{c}_1 + \vec{c}_2 \;(\bmod\; q_\ell)\)}.
    \item[Πολλαπλασιασμός:\quad
          $\symtt{Mult}_{evk}(\vec{c}_1, \vec{c}_2)$]\hfill\\
          Για \mbox{\(\vec{c}_1 = (b_1, a_1), \vec{c}_2 =
              (b_2, a_2) \in \Ring_{q_\ell}^2\)}, επιστρέφει
          \mbox{\(\vec{c}_{\symsf{mult}} \gets
              (d_0, d_1) + \lfloor p^{-1} \cdot d_2
              \cdot evk \rceil \;(\bmod\; q_\ell)\)},
          όπου \mbox{\((d_0, d_1, d_2) = (b_1 b_2, a_1 b_2
              + a_2 b_1, a_1 a_2) \;(\bmod\; q_\ell)\)}.
    \item[Ανακλιμάκωση:\quad
          $\symtt{RS}_{\ell\to\ell'}(\vec{c})$]\hfill\\
          Για \mbox{\(\vec{c} \in \Ring_{q_\ell}^2\)}
          και ένα επίπεδο $\ell' < \ell$, επιστρέφει
          \mbox{\(\vec{c}' \gets \Bigl\lfloor
              \frac{q_{\ell'}}{q_\ell}\vec{c}
              \Bigr\rceil \;(\bmod\; q_{\ell'})\)}.
\end{description}
