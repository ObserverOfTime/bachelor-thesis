/*!
 * \file
 * \brief %Multiplier class implementation
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include "multiplier.hpp"

#include "linear_transformer.hpp"

seal::Ciphertext Multiplier::operator()(
      Operands &ciphertexts, uint64_t size, const Mat1P &sigma,
      const Mat1P &tau, const Mat2P &phi, const Mat2P &psi) {
    seal::SEALContext ctx(params_);
    seal::Evaluator eval(ctx);
    LinearTransformer transform(params_, keys_);

    seal::Ciphertext product, temp;
    Mat1C ct_a(size), ct_b(size);

    auto ct1 = matrix::crypto::flatten(eval, keys_, ciphertexts.first);
    auto ct2 = matrix::crypto::flatten(eval, keys_, ciphertexts.second);

    ct_a[0] = transform(ct1, sigma);
    ct_b[0] = transform(ct2, tau);

    eval.multiply(ct_a[0], ct_b[0], product);
    eval.mod_switch_to_next_inplace(product);

    for (uint64_t k = 1; k < size; ++k) {
        ct_a[k] = transform(ct_a[0], phi[k - 1]);
        eval.rescale_to_next_inplace(ct_a[k]);
        stabilise_scale(ct_a[k]);

        ct_b[k] = transform(ct_b[0], psi[k - 1]);
        eval.rescale_to_next_inplace(ct_b[k]);
        stabilise_scale(ct_b[k]);

        eval.multiply(ct_a[k], ct_b[k], temp);
        eval.add_inplace(product, temp);
    }

    return product;
}
