/*!
 * \file
 * \brief CLI arguments
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include "quick_arg_parser.hpp"

/*! \brief %Arguments struct */
struct Arguments : MainArguments<Arguments> {
    /*! \brief The matrix size value */
    uint64_t size = option("size", 's', "The size of the matrices.") = 3;

    /*! \brief The lower bound value */
    double lower = option("lower", 'l', "The lower bound of the values.") = .1;

    /*! \brief The upper bound value */
    double upper = option("upper", 'u', "The upper bound of the values.") = 10;

    /*! \brief The verbosity flag */
    bool verbose = option("verbose", 'v', "Increase log verbosity.") = false;

#ifdef PROJECT_VERSION
    /*! \brief The program's version string */
    static inline std::string version = PROJECT_VERSION;  // NOLINT
#endif  // PROJECT_VERSION

    /*!
     * \brief Define the help output
     * \param programName The program's name
     * \return The program's usage text
     */
    static std::string help(const std::string &programName) {
        return "Usage:\n\n" + programName + " [OPTIONS]\n\nOptions:\n";
    }

    /**! \brief Dummy version option */
    std::optional<bool> __version =
          option("version", 'V', "Print the program version and exit.");

    /**! \brief Dummy help option */
    std::optional<bool> __help =
          option("help", '?', "Print this help message and exit.");
};  // struct Arguments
