/*!
 * \file
 * \brief Matrix utility implementations
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#include "utils.hpp"

#include <cassert>
#include <random>

void matrix::random(Mat2d &mat, double lower, double upper) {
    assert(lower < upper);
    std::minstd_rand gen(std::random_device {}());
    std::uniform_real_distribution dist(lower, upper);
    for (auto i = mat.begin(); i != mat.end(); ++i)  // NOLINT
        for (auto j = i->begin(); j != i->end(); ++j) *j = dist(gen);
}

Mat1d matrix::diagonal(const Mat2d &mat, uint32_t n) {
    uint32_t k = 0, l = mat.size();
    Mat1d diagonal(l);
    for (uint32_t i = 0, j = n; i < l - n && j < l; ++i, ++j)
        diagonal[k++] = mat[i][j];
    for (uint32_t i = l - n, j = 0; i < l && j < n; ++i, ++j)
        diagonal[k++] = mat[i][j];
    return diagonal;
}

Mat2d matrix::diagonals(const Mat2d &mat) {
    uint32_t l = mat.size();
    Mat2d diagonals(l);
    for (uint32_t i = 0; i < l; ++i) diagonals[i] = diagonal(mat, i);
    return diagonals;
}

Mat2d matrix::multiply(const Mat2d &left, const Mat2d &right) {
    uint32_t l = left.size();
    Mat2d result(l, Mat1d(l));
    for (uint32_t i = 0; i < l; ++i)
        for (uint32_t j = 0; j < l; ++j)
            for (uint32_t k = 0; k < l; ++k)
                result[i][j] += left[i][k] * right[k][j];
    return result;
}
