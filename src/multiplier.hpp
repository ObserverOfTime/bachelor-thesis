/*!
 * \file
 * \brief %Multiplier class declaration
 * \author Ioannis Somos
 * \copyright Microsoft Public License
 */

#pragma once

#include "matrix/crypto.hpp"

/*! \brief %Multiplier class */
class Multiplier final {
public:
    /*! \brief The operands of the multiplication */
    using Operands = const std::pair<Mat1C, Mat1C>;

    /*!
     * \brief Create an instance of a %Multiplier
     * \param params the encryption parameters used for the multiplication
     * \param keys the Galois keys used for the multiplication
     */
    explicit Multiplier(
          const seal::EncryptionParameters &params,
          const seal::GaloisKeys &keys):
        params_(params), keys_(keys) {}

    /*!
     * \brief Multiply a pair of ciphertexts
     * \param ciphertexts the ciphertexts to be multiplied
     * \param size the size of the encrypted matrix
     * \param sigma the diagonals of the &sigma; permutation
     * \param tau the diagonals of the &tau; permutation
     * \param phi the diagonals of the &phi; permutation
     * \param psi the diagonals of the &psi; permutation
     * \return The generated \c seal::Ciphertext
     * \see matrix::permute
     */
    seal::Ciphertext operator()(
          Operands &ciphertexts,
          uint64_t size,
          const Mat1P &sigma,
          const Mat1P &tau,
          const Mat2P &phi,
          const Mat2P &psi);

    /*!
     * \brief Stabilise the scale of a \c seal::Ciphertext
     * \param ciphertext the ciphertext to be rescaled
     */
    static inline void stabilise_scale(seal::Ciphertext &ciphertext) {
        ciphertext.scale() = pow(ENCODING_SCALE, 2);
    }

private:
    /*! \brief The encryption parameters of the multiplier */
    const seal::EncryptionParameters &params_;

    /*! \brief The Galois keys used by the multiplier */
    const seal::GaloisKeys &keys_;
};  // class Multiplier
